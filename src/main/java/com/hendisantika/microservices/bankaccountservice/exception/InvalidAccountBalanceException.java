package com.hendisantika.microservices.bankaccountservice.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : Bank Account Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/05/21
 * Time: 08.42
 */
public class InvalidAccountBalanceException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InvalidAccountBalanceException(String msg) {
        super(msg);
    }

}
