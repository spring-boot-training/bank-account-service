package com.hendisantika.microservices.bankaccountservice.controller;

import com.hendisantika.microservices.bankaccountservice.model.BankAccount;
import com.hendisantika.microservices.bankaccountservice.service.BankAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : Bank Account Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/05/21
 * Time: 08.49
 */
@RestController
@Slf4j
@RequestMapping("/bank-account")
public class BankAccountController {

    @Autowired
    public BankAccountService bankAccountService;

    @PostMapping
    public ResponseEntity<?> createBankAccount(@RequestBody BankAccount bankAccount, HttpServletRequest request) throws URISyntaxException {

        bankAccountService.createBankAccount(bankAccount);

        log.info("created bank account {}", bankAccount);

        URI uri = new URI(request.getRequestURL() + "bank-account/" + bankAccount.getAccountId());

        return ResponseEntity.created(uri).build();
    }

    @GetMapping
    public ResponseEntity<List<BankAccount>> getAllBankAccounts() {
        List<BankAccount> bankAccounts = bankAccountService.retrieveAllBankAccounts();

        log.info("retrieved All bank accounts {}", bankAccounts);

        return ResponseEntity.ok(bankAccounts);
    }

    @GetMapping("/{accountId}")
    public ResponseEntity<BankAccount> getBankAccount(@PathVariable("accountId") String accountId) {

        BankAccount account = bankAccountService.retrieveBankAccount(accountId);

        log.info("retrieved bank account {}", account);

        return ResponseEntity.ok(account);
    }
}
