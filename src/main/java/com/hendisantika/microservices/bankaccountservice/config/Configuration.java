package com.hendisantika.microservices.bankaccountservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : Bank Account Service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/05/21
 * Time: 08.34
 */
@Component
@ConfigurationProperties(prefix = "bank-account-service")
@Data
public class Configuration {

    private Double minBalance;

    private Double maxBalance;
}
